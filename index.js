let x = 10;
let kata2 = ' angkanya';
console.log("Nilai x adalah : " + x + kata2 +" nya")

// Assignment operator kombinasi dengan operator lain

let y = 10;
y += 10
// ~~~~~~~~~ pinggir angka 1
console.log(`Nilai y adalah : ${y} angka nya`)

// Array
let arr = ['seu','imam','rima','fiqri','alfayruz']

// Object
let obj = {
	name : 'imam',
	age : 20
}

// Array of Object
let studentData = [
	{
		nama : 'Seu',
		umur : '21 Tahun',
		relasi : {
			ayah : 'Kamizan',
			ibu : 'Asnah',
			adik : ['Dhanel','Isma']
		}
	},
	{
		nama : 'Dinney',
		umur : '20 Tahun',
		relasi : {
			ayah : 'Azmi',
			ibu : 'Intan',
			adik : ['Maysarah','Siti'],
			teman : {
				game : 'Nanang',
				ngoding : 'Dini'
			}
		}
	},
	{
		nama : 'Nopal',
		umur : '20 Tahun',
		relasi : {
			ayah : 'Jeni',
			ibu : 'Lisa',
			adik : ['Maimunah','Intan']
		}
	}
]

// 1. Print nama adik kedua dari student bernama Seu
let adikSeu;
for (let i = 0; i < studentData.length; i++) {
	if (studentData[i].nama == "Seu") {
		adikSeu = studentData[0].relasi.adik[1]
		break
	}
}
console.log(adikSeu)

// Cara lain solve nya dengan pake array method
// const studentdata = studentData.find(data => data.nama === 'Seu')
// console.log(studentdata.relasi.adik[1])

// const studentdata = studentData.find(data => {
// 	console.log(data)
// 	data.nama === 'Seu'
// })

const studentdata = studentData.find(data => {
	// console.log(data.nama)
	if (data.nama === 'Dinney') {
		data.relasi.adik[0] = 'Sarah'
		data.relasi.teman.game = 'Seu'
	}
	return data.nama === 'Dinney'
})
console.log(studentdata)
